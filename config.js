module.exports = {
  siteMetadata: {
    name: 'Gatsby CMS',
    title: 'Gatsby CMS',
    description: 'An All-in-One solution for Modern Content Creation',
    siteUrl: 'http://localhost:8000'
  },

  blog: {
    blogPagesDirectory: `${__dirname}/content/blog/`
  },

  contentPaths: {
    blog: 'content/blog'
  },

  slugs: {
    tagPrefix: '/blog/tag',
    categoryPrefix: '/blog/category',
    authorPrefix: '/blog/author'
  }
}
