import React, { Fragment } from 'react'
import Helmet from 'react-helmet'

import Header from './layout/Header'
import Footer from './layout/Footer'

import '../styles/main.scss'

import helmetConfig from '../helmetConfig'

export default ({ children, location }) => (
  <Fragment>
    <Helmet {...helmetConfig.head} />
    <Header path={location.pathname} />
    {children}
    <Footer />
  </Fragment>
)
