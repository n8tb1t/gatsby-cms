import React from 'react'
import Helmet from 'react-helmet'
import Layout from '../components/Layout'

export default props => (
  <Layout location={props.location}>
    <main className="notfound">
      <Helmet title="404" />
    </main>
  </Layout>
)
