const createBlogPages = require('./create-pages/create-blog-pages')

module.exports = async ({ graphql, actions }) => {
  await createBlogPages(graphql, actions)

}
